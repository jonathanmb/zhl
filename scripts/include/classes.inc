use uo;

include "include/spelldata";
//include "include/attributes";
include "include/itemutil";
include "include/client";
include "include/classitems";
include "include/class_bonuses";
include "include/races";

const CLASSEID_BARD       := "IsBard";
const CLASSEID_CRAFTER    := "IsCrafter";
const CLASSEID_MAGE       := "IsMage";
const CLASSEID_RANGER     := "IsRanger";
const CLASSEID_THIEF      := "IsThief";
const CLASSEID_WARRIOR    := "IsWarrior";
const CLASSEID_POWERPLAYER  := "IsPowerPlayer";

//const LEVEL1 := 360;
//const LEVEL2 := 460;
//const LEVEL3 := 560;
//const LEVEL4 := 660;
//const LEVEL5 := 760;
//const LEVEL6 := 860;
//const LEVEL7 := 960;
//const LEVEL8 := 1060;

//const PERCENT1 := 60;
//const PERCENT2 := 65;
//const PERCENT3 := 70;
//const PERCENT4 := 75;
//const PERCENT5 := 80;
//const PERCENT6 := 85;
//const PERCENT7 := 90;
//const PERCENT8 := 95;

const EACH_MUST_REPRESENT := 7.5;
const REPRESENT_LEVEL_MOD := 1;
const AVERAGE_SKILL       := 75;
const AVERAGE_LEVEL_MOD   := 15;
const PPSKILLNUMBER 	  := 48;

var counter:=0;

function ClasseBonus( who, classeid )

	var level := CDbl(GetObjProperty( who, classeid ) );

	if( level )
		return( 1 + BONUS_PER_LEVEL * level );
	else
		return( 1 );
	endif

endfunction


function ClasseBonusByLevel( level )

	return CDbl( 1.0 + ( BONUS_PER_LEVEL * CDbl(level) ) );

endfunction


function ClasseBonusBySkillId( who, skillid )

	return( ClasseBonusByLevel( IsSpecialisedIn( who, skillid ) ) );

endfunction


function EnumerateRestrictedItemTypesFromClasse( classeid )

	var fract_array := {};
	var restricted_types := {};

	case( classeid )

		CLASSEID_BARD:		fract_array.append( GetPlatemailArmorGraphics() );
					fract_array.append( GetShieldGraphics() );
					break;

		CLASSEID_CRAFTER:	fract_array.append( GetChainmailArmorGraphics() );
					fract_array.append( GetPlatemailArmorGraphics() );
					break;

		CLASSEID_MAGE:		fract_array.append( GetLeatherArmorGraphics() );
					fract_array.append( GetStuddedLeatherArmorGraphics() );
					fract_array.append( GetBoneArmorGraphics() );
					fract_array.append( GetRingmailArmorGraphics() );
					fract_array.append( GetChainmailArmorGraphics() );
					fract_array.append( GetPlatemailArmorGraphics() );
					fract_array.append( GetShieldGraphics() );
					break;

		CLASSEID_RANGER:	fract_array.append( GetPlatemailArmorGraphics() );
					fract_array.append( GetShieldGraphics() );
					break;

		CLASSEID_THIEF:		fract_array.append( GetChainmailArmorGraphics() );
					fract_array.append( GetPlatemailArmorGraphics() );
					fract_array.append( GetShieldGraphics() );
					break;
					
		endcase


	foreach type in fract_array
		foreach graphic in type
			restricted_types.append( graphic );
		endforeach
	endforeach

	return restricted_types;

endfunction


function GetClasseIdForSkill( skillid )

	foreach id in GetClasseIds()
		if( skillid in GetClasseSkills( id ) )
			return id;
		endif
	endforeach

	return 0;

endfunction


function GetClasseIds()

	return { CLASSEID_BARD,
		 CLASSEID_CRAFTER,
		 CLASSEID_MAGE,
		 CLASSEID_RANGER,
		 CLASSEID_THIEF,
		 CLASSEID_WARRIOR,
		 CLASSEID_POWERPLAYER };

endfunction


function GetClasseName( classeid )

        case( classeid )

                CLASSEID_BARD:		return( "Bard" );
                CLASSEID_CRAFTER:	return( "Crafter" );
                CLASSEID_MAGE:		return( "Mage" );
                CLASSEID_RANGER:	return( "Ranger" );
                CLASSEID_THIEF:		return( "Thief" );
                CLASSEID_WARRIOR:	return( "Warrior" );
                CLASSEID_POWERPLAYER:	return( "Power Player" );
		default:		return( "INVALID" );

        endcase

endfunction


function GetClasseSkills( classeid )

	case( classeid )

		CLASSEID_BARD:		return { SKILLID_BEGGING,
						 SKILLID_CARTOGRAPHY,
						 SKILLID_ENTICEMENT,
						 SKILLID_HERDING,
						 SKILLID_MUSICIANSHIP,
						 SKILLID_PEACEMAKING,
						 SKILLID_PROVOCATION,
						 SKILLID_TASTEID	};

		CLASSEID_CRAFTER:	return { SKILLID_ARMSLORE,
						 SKILLID_BLACKSMITHY,
						 SKILLID_BOWCRAFT,
						 SKILLID_CARPENTRY,
						 SKILLID_LUMBERJACKING,
						 SKILLID_MINING,
						 SKILLID_TAILORING,
						 SKILLID_TINKERING	};

		CLASSEID_MAGE:		return { SKILLID_ALCHEMY,
						 SKILLID_EVALINT,
						 SKILLID_INSCRIPTION,
						 SKILLID_ITEMID,
						 SKILLID_MAGERY,
						 SKILLID_MEDITATION,
						 SKILLID_MAGICRESISTANCE,
						 SKILLID_SPIRITSPEAK	};

		CLASSEID_RANGER:	return { SKILLID_ANIMALLORE,
						 SKILLID_TAMING,
						 SKILLID_ARCHERY,
						 SKILLID_CAMPING,
						 SKILLID_COOKING,
						 SKILLID_FISHING,
						 SKILLID_TRACKING,
						 SKILLID_VETERINARY	};

		CLASSEID_THIEF:		return { SKILLID_DETECTINGHIDDEN,
						 SKILLID_HIDING,
						 SKILLID_LOCKPICKING,
						 SKILLID_POISONING,
						 SKILLID_REMOVETRAP,
						 SKILLID_SNOOPING,
						 SKILLID_STEALING,
						 SKILLID_STEALTH	};

		CLASSEID_WARRIOR:	return { SKILLID_ANATOMY,
						 SKILLID_FENCING,
						 SKILLID_HEALING,
						 SKILLID_MACEFIGHTING,
						 SKILLID_PARRY,
						 SKILLID_SWORDSMANSHIP,
						 SKILLID_TACTICS,
						 SKILLID_WRESTLING	};
		 
		CLASSEID_POWERPLAYER: 
					return { SKILLID_BEGGING,
						 SKILLID_CARTOGRAPHY,
						 SKILLID_ENTICEMENT,
						 SKILLID_HERDING,
						 SKILLID_MUSICIANSHIP,
						 SKILLID_PEACEMAKING,
						 SKILLID_PROVOCATION,
						 SKILLID_TASTEID, 
						 SKILLID_ARMSLORE,
						 SKILLID_BLACKSMITHY,
						 SKILLID_BOWCRAFT,
						 SKILLID_CARPENTRY,
						 SKILLID_LUMBERJACKING,
						 SKILLID_MINING,
						 SKILLID_TAILORING,
						 SKILLID_TINKERING,
						 SKILLID_ALCHEMY,
						 SKILLID_EVALINT,
						 SKILLID_INSCRIPTION,
						 SKILLID_ITEMID,
						 SKILLID_MAGERY,
						 SKILLID_MEDITATION,
						 SKILLID_MAGICRESISTANCE,
						 SKILLID_SPIRITSPEAK,
						 SKILLID_ANIMALLORE,
						 SKILLID_TAMING,
						 SKILLID_ARCHERY,
						 SKILLID_CAMPING,
						 SKILLID_COOKING,
						 SKILLID_FISHING,
						 SKILLID_TRACKING,
						 SKILLID_VETERINARY,
						 SKILLID_DETECTINGHIDDEN,
						 SKILLID_HIDING,
						 SKILLID_LOCKPICKING,
						 SKILLID_POISONING,
						 SKILLID_REMOVETRAP,
						 SKILLID_SNOOPING,
						 SKILLID_STEALING,
						 SKILLID_STEALTH,
						 SKILLID_ANATOMY,
						 SKILLID_FENCING,
						 SKILLID_HEALING,
						 SKILLID_MACEFIGHTING,
						 SKILLID_PARRY,
						 SKILLID_SWORDSMANSHIP,
						 SKILLID_TACTICS,
						 SKILLID_WRESTLING,
						 SKILLID_FORENSICS};
						 

		default:		return {};

	endcase

endfunction


function GetGeneralSkills()

	return { SKILLID_BEGGING,
		 SKILLID_ARMSLORE,
		 SKILLID_MAGICRESISTANCE,
		 SKILLID_ARCHERY,
		 SKILLID_HIDING,
		 SKILLID_TACTICS	};

endfunction


function GetMaxCircle( who )

	foreach classeid in GetClasseIds()
		if( GetObjProperty( who, classeid ) )
			return GetMaxCircleForClasse( classeid );
		endif
	endforeach

	return 0;

endfunction


function GetMaxCircleForClasse( classeid )

	case( classeid )

		CLASSEID_BARD:		return 10;

		CLASSEID_CRAFTER:	return 6;

		CLASSEID_MAGE:		return 0;

		CLASSEID_RANGER:	return 8;

		CLASSEID_THIEF:		return 8;
		
		CLASSEID_WARRIOR:	return 4;
		
		CLASSEID_POWERPLAYER: 	return 0;

		default:		return 0;

	endcase

endfunction


function GetSkillPointsMultiplier( who, skillid )

	return ClasseBonusByLevel( IsSpecialisedIn( who, skillid ) );

endfunction


function GetStatPointsMultiplier( who, stat )

	var level   := HaveStatAffinity( who, stat );
	var mult    := 1.0;
	if( level )
		mult  := ClasseBonusByLevel( level );
	else
		level := HaveStatDifficulty( who, stat );
		if( level )
			mult := CDbl( 1.0 / ClasseBonusByLevel( level ) );
		endif
	endif

	return mult;

endfunction


function HaveInvalidSkillEnchantmentForClasse( item, classeid )
         
	var ret	:= 0;
	var skillnum	:= GetObjProperty( item, "skilladv" );

	if( (skillnum) || (skillnum == 0) && !(skillnum in GetGeneralSkills()) )
		if( !(skillnum in GetClasseSkills( classeid )) )
			ret := 1;
		endif
	endif

	return ret;
 
        return 0;

endfunction


function HaveRestrictedModEnchantmentForClasse( who, item, classeid )

	var ret := 0;
	case( classeid )

		CLASSEID_BARD:		break;

		CLASSEID_CRAFTER:	break;

		CLASSEID_MAGE:		break;

		CLASSEID_RANGER:	break;

		CLASSEID_THIEF:		if( GetObjProperty( item, "MagicImmunity" ) )
						ret := 1;
					elseif( GetObjProperty( item, "SpellReflect" ) )
						ret := 1;
					elseif( GetObjProperty( item, "PermMagicImmunity" ) )
						ret := 1;
					elseif( GetObjProperty( item, "Permmr" ) )
						ret := 1;
					endif
					break;
		
		CLASSEID_POWERPLAYER: 	break;
		
		CLASSEID_WARRIOR:	if( GetObjProperty( item, "MagicImmunity" ) )
						ret := 1;
					elseif( GetObjProperty( item, "SpellReflect" ) )
						ret := 1;
					elseif( GetObjProperty( item, "PermMagicImmunity" ) )
						ret := 1;
					elseif( GetObjProperty( item, "Permmr" ) )
						ret := 1;
					endif
					break;

		endcase
	
	return ret;

endfunction


function HaveStatAffinity( who, stat )

	var classeids;

	case( stat )
		"str":	classeids := {  CLASSEID_CRAFTER,
					CLASSEID_WARRIOR};	break;

		"dex":	classeids := {  CLASSEID_BARD,
					CLASSEID_THIEF  };	break;

		"int":	classeids := {  CLASSEID_BARD,
					CLASSEID_MAGE	};	break;

		default:	classeids := {};
	endcase

	foreach id in classeids
		var level := CInt( GetObjProperty( who, id ) );
		if( level )
			return level;
		endif
	endforeach

	return 0;

endfunction


function HaveStatDifficulty( who, stat )

	var classeids, level;

	case( stat )
		"str":
			classeids := {	CLASSEID_BARD,
					CLASSEID_MAGE	}; break;
		"dex":
			classeids := {	};	break;
		"int":
			classeids := {	CLASSEID_CRAFTER,
					CLASSEID_WARRIOR};	break;
		default:
			classeids := {			};
	endcase

	foreach id in classeids
		level := CInt( GetObjProperty( who, id ) );
		if( level )
			return level;
		endif
	endforeach

	return 0;

endfunction


function IsSpecialisedIn( who, skillid )

	var classeid := GetClasseIdForSkill( skillid );
	if( classeid )
		return CInt( GetObjProperty( who, classeid ) );
	endif

endfunction


function IsBard( who )
	return IsFromThatClasse( who, GetClasseSkills( CLASSEID_BARD ) );
endfunction
function IsCrafter( who )
	return IsFromThatClasse( who, GetClasseSkills( CLASSEID_CRAFTER ) );
endfunction
function IsMage( who )
	return IsFromThatClasse( who, GetClasseSkills( CLASSEID_MAGE ) );
endfunction
function IsRanger( who )
	return IsFromThatClasse( who, GetClasseSkills( CLASSEID_RANGER ) );
endfunction
function IsThief( who )
	return IsFromThatClasse( who, GetClasseSkills( CLASSEID_THIEF ) );
endfunction
function IsWarrior( who )
	return IsFromThatClasse( who, GetClasseSkills( CLASSEID_WARRIOR ) );
endfunction
function IsPowerPlayer(who)
	return IsFromThatClasse( who, GetClasseSkills( CLASSEID_POWERPLAYER ) );
endfunction


function IsFromThatClasse( who, classe_skills )

	var classe	:= 0;
	var total	:= 0;
	var number := len( classe_skills );


	for i := 0 to SKILLID__HIGHEST
		var amount := GetEffectiveSkill( who, i );
		total := total + amount;
		if( i in classe_skills )
			classe := classe + amount;
		endif
	endfor

	/*Does a fast check on Powerplayers because their levels are determined slightly differently*/
	
	if (number == PPSKILLNUMBER)
		if(classe>=5880)
			return 4;
		elseif(classe>=5145)
			return 3;
		elseif(classe>=4410)
			return 2;
		elseif(classe>=3675)
			return 1;
		endif
		return 0;
	endif

	if( classe < AVERAGE_SKILL * number )
		return 0;
	elseif( classe < CInt(total * number * EACH_MUST_REPRESENT * 0.01) )
		return 0;
		
	else
		var level     := 1;
		var represent := EACH_MUST_REPRESENT + REPRESENT_LEVEL_MOD;
		var percent   := CInt( total * number * represent * 0.01 );
		var average_t := CInt( (AVERAGE_SKILL + AVERAGE_LEVEL_MOD) * number );
		while( (classe >= average_t) && (classe >= percent) )
			level     := level + 1;
			represent := CDbl( represent + REPRESENT_LEVEL_MOD );
			percent   := CInt( total * number * represent * 0.01 );
			average_t := CInt( average_t + AVERAGE_LEVEL_MOD * number );
		endwhile

		return level;
	endif

endfunction

function IsProhibitedClasseItem(who, item)
	var item_level, player_level;
	
	foreach CLASSEID in GetClasseIds()
		item_level := CInt(GetObjProperty(item, CLASSEID));
		if (item_level)
			player_level := CInt(GetObjProperty(who, CLASSEID));
			if (item_level>0)	
				return (player_level < item_level);       // If the item level is positive
			else					          // player level >= item level to wear.

				return (player_level >= abs(item_level)); // If the item level is negative 
			endif						  // the player level must be less than the
		endif							  // absolute value of that to wear it.
	endforeach
endfunction

function IsProhibitedByClasse( who, item )

	// *** NOTE *** Item mod restrictions commented out for ES system
	var ret := 0;
	
	if( GetObjProperty( who, CLASSEID_BARD	) )
		if( HaveInvalidSkillEnchantmentForClasse( item, CLASSEID_BARD ) )
			ret := 1;
		elseif( item.graphic in EnumerateRestrictedItemTypesFromClasse( CLASSEID_BARD ) )
			ret := 1;
		endif

	elseif( GetObjProperty( who, CLASSEID_CRAFTER ) )
		if( HaveInvalidSkillEnchantmentForClasse( item, CLASSEID_CRAFTER ) )
			ret := 1;
		elseif( GetObjProperty( item, "str" ) )
			ret := 1;
		elseif( item.graphic in EnumerateRestrictedItemTypesFromClasse( CLASSEID_CRAFTER ) )
			ret := 1;
		endif

	elseif( GetObjProperty( who, CLASSEID_MAGE ) )
		if( HaveInvalidSkillEnchantmentForClasse( item, CLASSEID_MAGE ) )
			ret := 1;
		elseif( GetObjProperty( item, "str" ) )
			ret := 1;
		//elseif( GetObjProperty( item, "ArBonus" ) )
		//	ret := 1;
		elseif( item.graphic in EnumerateRestrictedItemTypesFromClasse( CLASSEID_MAGE ) )
			ret := 1;
		endif

	elseif( GetObjProperty( who, CLASSEID_RANGER ) )
		if( HaveInvalidSkillEnchantmentForClasse( item, CLASSEID_RANGER ) )
			ret := 1;
		elseif( item.graphic in EnumerateRestrictedItemTypesFromClasse( CLASSEID_RANGER ) )
			ret := 1;
		endif

	elseif( GetObjProperty( who, CLASSEID_THIEF ) )
		if( HaveInvalidSkillEnchantmentForClasse( item, CLASSEID_THIEF ) )
			ret := 1;
		elseif( item.graphic in EnumerateRestrictedItemTypesFromClasse( CLASSEID_THIEF ) )
			ret := 1;
		endif

	elseif( GetObjProperty( who, CLASSEID_WARRIOR ) )
		if( HaveInvalidSkillEnchantmentForClasse( item, CLASSEID_WARRIOR ) )
			ret := 1;
		elseif( GetObjProperty( item, "int" ) )
			ret := 1;
		elseif( item.graphic in EnumerateRestrictedItemTypesFromClasse( CLASSEID_WARRIOR ) )
			ret := 1;
		elseif( HaveRestrictedModEnchantmentForClasse( who, item, CLASSEID_WARRIOR ) )
			ret := 1;
		endif
		
		
		endif
						
	return ret;

endfunction


function AssignClasse( character )

	var t;

	foreach classe in GetClasseIds()
		t := IsFromThatClasse( character, GetClasseSkills( classe ) );
		if( t )
			SetObjProperty( character, classe, t );
		endif
	endforeach

endfunction

function GetClass(who)
	//ripped and modified from arch :) ... returns current class
	foreach key in GetClasseIds()
		if (GetObjProperty(who, key))
			return key;
		endif
	endforeach
	return 0; //no class found

endfunction

// New version of IsFromThatClasse - returns CLASSEID if classed, 0 otherwise
function SetClasse (who)

        var percentage:=0;
        var classtotal:=0;
        var playerclass:=0;
        var classlist := GetClasseIds();
        var skilltotal := GetSkillTotal(who);
        var tempclasstotal;
        // get class
        foreach class in classlist
          if (class != CLASSEID_POWERPLAYER)
            tempclasstotal:=GetClasseSkillTotal(who, class);
            if (tempclasstotal > classtotal)
               playerclass:= class;
               classtotal:= tempclasstotal;
            endif
            //broadcast(class+" checked");
          endif
        endforeach
        percentage:=GetPercentage(classtotal, skilltotal);
        // get level
        var level:=0;
	if (skilltotal >= 3675)
		level:=1;
	        if (skilltotal >= 5880)
        		level:=4;
        	elseif (skilltotal >= 5145)
           		level:=3;
        	elseif (skilltotal >= 4410)
           		level:=2;
		endif
           	playerclass:=CLASSEID_POWERPLAYER;		
        
        endif

        // apply race bonus
        if (GetObjProperty(who, GetRaceProtType(who)) < GetRaceProtBonus(who))
        	SetRaceProt(who);
        endif
        
        // set class
        if (level)
           foreach class in classlist
                  EraseObjProperty(who, class);
           endforeach
           SetObjProperty(who, playerclass, level);
           return playerclass;
        endif
        return; 
              
endfunction

function GetSkillTotal (who)
         var total:=0;
         for i:= 0 to SKILLID__HIGHEST
             total:=total + CDbl(GetEffectiveSkillBaseValue(who, i)) / 10;
         endfor
         return total;
endfunction

// returns the total skills in given class for that player
function GetClasseSkillTotal (who, class)

         var classtotal:=0;
         var skills:=GetClasseSkills(class);
         for i:= 0 to SKILLID__HIGHEST
             if (i in skills)
                   classtotal:=classtotal+ CDbl(GetEffectiveSkillBaseValue(who, i)) /10;
             endif

         endfor

        // broadcast(class+": "+classtotal);
        return classtotal;
        
endfunction

// Returns the % of class skills to total skills for class
function GetClasseSkillPercentage(who, class)

         var percentage:=0;
         percentage:= (CDbl(GetClasseSkillTotal(who, class)) / CDbl(GetSkillTotal(who))) * 100;
          //        broadcast("SHOULDN'T SEE THIS");
         return percentage;

endfunction

// simpler version of GetClasseSkillPercentage to save on cpu time
function GetPercentage(classtotal, skilltotal)
         return (CDbl(classtotal) / CDbl(skilltotal)) * 100;
endfunction

// Returns class level (regardless of class)
function GetClasseLevel(who)
	
	var class:= GetClass(who);
	if (class)
		return GetObjProperty(who, class);
	else
		return 0;
	endif
	
endfunction


function GetResistanceMultiplier (who, dmgtype)

	var class := GetClass(who);
	var level := GetClasseLevel(who);
	
	var mult := GetElementalAffinityByClasse(class, dmgtype);
	return level * mult;
	
endfunction
							
function GetElementalAffinityByClasse(class, dmgtype)

	var affinity:=0;
	case (dmgtype)
		FIRE:	case(class)
				CLASSEID_MAGE:		affinity:=BONUS_PER_LEVEL;
				CLASSEID_WARRIOR:	affinity:=PENALTY_PER_LEVEL;
			endcase
		
		AIR:	case(class)
				CLASSEID_MAGE:		affinity:=BONUS_PER_LEVEL;
				CLASSEID_WARRIOR:	affinity:=PENALTY_PER_LEVEL;
			endcase		
			
		EARTH:	case(class)
				CLASSEID_MAGE:		affinity:=BONUS_PER_LEVEL;
				CLASSEID_WARRIOR:	affinity:=PENALTY_PER_LEVEL;
			endcase		
			
		WATER:	case(class)
				CLASSEID_MAGE:		affinity:=BONUS_PER_LEVEL;
				CLASSEID_WARRIOR:	affinity:=PENALTY_PER_LEVEL;
			endcase	
				
		NECRO:	case(class)
				CLASSEID_MAGE:		affinity:=BONUS_PER_LEVEL;
				CLASSEID_WARRIOR:	affinity:=PENALTY_PER_LEVEL;
			endcase		
		ACID:	case(class)
				CLASSEID_MAGE:		affinity:=BONUS_PER_LEVEL;
				CLASSEID_WARRIOR:	affinity:=PENALTY_PER_LEVEL;
			endcase	
	endcase
	
	return affinity;
	
endfunction

