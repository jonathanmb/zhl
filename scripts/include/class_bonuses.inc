/* Barnabus 
Contains constants for class bonuses
and documentation on bonuses (if relevant)
*/

/*
Notes:
fields.inc -- Mages get TrueMage cprop on fields.

hitscriptinc.inc -- 
ModByDist - Rangers get +1 max range per level before base damage is 1/4 (normally 10). //now RANGER_LEVEL_ARCHERY_RANGE_BONUS
	Rangers do basedamage * ClasseBonus(level) to NPCs if range > 1 and weapon uses Archery
	Warriors do basedamage * ClasseBonus(level) to NPCs. 
	Warriors do basedamage * ClasseBonusPerLevel(level-2) against players.
	Mages have damage they do basedamage/ClasseBonus(level)
	Mages have damage they receive basedamage * ClasseBonus( defender , CLASSEID_MAGE );

DealDamage -
	Warriors take less damage from NPCs: rawdamage//lasseBonus(level)
	Warriors take less damage from players: rawdamage/ClasseBonus(level-1) and basedamage/ClasseBonus(level-1)

spelldata.inc
BurnSelf-
	Mages do less damage to self: dmg := CInt( dmg / ClasseBonus( caster, CLASSEID_MAGE ) );
	Warriors do more: dmg := CInt( dmg * ClasseBonus( caster, CLASSEID_WARRIOR ) );

IsProtected( caster, cast_on, circle )-
	Warriors find it harder to be protected: circle * ClasseBonus( cast_on, CLASSEID_WARRIOR );

ModifyWithMagicEfficiency( who, val )-
	Mages have typical per level bonus ClasseBonus(level)
	Warrior have per level penalty
	Bards gain bonus if using bard instrument

Resisted( caster, cast_on, circle, dmg )
	Mages have typical per level bonus ClasseBonus(level). Their spells harder to resist against, 
	easier for them to resist others. Take less damage after resist.
	Warrior have per level penalty. Their spells easier to resist, harder for them to resist others. 
	Take more damage after resist.

ResistedDefinedSkillGain - 
	Mages have typical per level bonus ClasseBonus(level)
	Warrior have per level penalty

TryToCast - 
	Mages cast faster: spelldelay := CInt( spelldelay / ClasseBonus( caster, CLASSEID_MAGE ) );
	Warriors cast slower:  spelldelay := CInt( spelldelay * ClasseBonus( caster, CLASSEID_WARRIOR ) );
	
//New generic functions could be used instead of some of the above (also in spelldata.inc)
ModifyWithNewClassMagicEfficiency
NewClassSpellCast (new version of TryToCast)

repair.inc
repair_item( character, item, skillid )
	Crafters can repair easier:
	diff := CInt(diff - GetObjProperty(character, CLASSEID_CRAFTER)* 2.5); //modifed so uses CRAFTER_REPAIR_DIFF_BONUS
	and get bonus to arms lore (armslore := CInt( armslore * ClasseBonus( character, CLASSEID_CRAFTER ) );)

vet.inc
TryToVetCure
	Rangers are faster:	delay := CInt( delay / CLASSE_BONUS );
	
TryToVetHeal
	Rangers are faster and heal more: 		delay := CInt( delay / CLASSE_BONUS ); healed := CInt( healed * CLASSE_BONUS );
	
TryToVetRes
	Only Rangers can do this
*/


const CLASSE_BONUS        := 1.5;
const BONUS_PER_LEVEL     := 0.2;  //used in the function ClasseBonus( who, classeid ) 
const LARGE_BONUS_PER_LEVEL  := 0.5;  
const PENALTY_PER_LEVEL := -0.2;
const LARGE_PENALTY_PER_LEVEL := -0.5;

const RANGER_LEVEL_ARCHERY_RANGE_BONUS := 1; //1 range per level further range before 1/4 base damage done

const CRAFTER_REPAIR_DIFF_BONUS := 2.5;

//SPELL CASTING BONUSES
const PURE_SPELLCASTER_BONUS := 40;
const SEMI_SPELLCASTER_BONUS := 20;

