use uo;
use os;

function GetRaceProtType(who)

	var race:= who.title_race;
	var racecfg:= ReadConfigFile ("::races");	
	if (!racecfg)
		//broadcast(racecfg);
	endif
	
	var elem:= FindConfigElem(racecfg, race);
	return elem.Prot;
	
endfunction

function GetRaceProtAmount(who)

	var race:=who.title_race;
	var racecfg:= ReadConfigFile ("::races");
	if (!racecfg)
		//broadcast(racecfg);
	endif
	
	var elem:= FindConfigElem(racecfg, race);
	if (!elem)
		return 0;
	endif
	
	return elem.PerLevel;
	
endfunction

function GetRaceProtBonus (who)

	var perlvl:= GetRaceProtAmount(who);
	return CInt(GetClasseLevel(who) * perlvl);

endfunction

function SetRaceProt(who)

	// WARNING - careful where you use this function as it doesn't
	// check if player is wearing an item with a better prot
	//SetObjProperty (who, GetRaceProtType(who), GetRaceProtBonus(who));
	
	return;
	
endfunction
