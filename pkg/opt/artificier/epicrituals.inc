function CheckReagents(caster, ConsumableTypes, Reserve := 1)

	var Reagents := array;
	var obj;
	foreach type in ConsumableTypes
		obj := FindObjtypeInContainer( caster.backpack, type );
		
		if(!obj || obj == error)
		
			PlayObjectCenteredEffect(caster, FX_SPELL_FIZZLE, SPEED_SPELL_FIZZLE, LOOP_SPELL_FIZZLE );
			PlaySoundEffect(caster, SFX_SPELL_FIZZLE);
			SendSysMessage(caster, "Insufficient Reagents.");
			sleep(2);
			
			if(Reserve)
				foreach locked in Reagents
					ReleaseItem(locked);
				endforeach
			endif
			
			return 0;
			
		else
			
			if(Reserve)
				if(!ReserveItem(obj))
					return 0; 
				endif
			endif
			
			Reagents.append(obj);
		endif
		
	endforeach
	
	return Reagents;
	
endfunction

function DestroyReagents( Reagents )

	foreach item in Reagents
		DestroyItem( item );
	endforeach

endfunction