Ritual DrawCircle

	Great cosmic energy, I humbly call upon your aid
	Facing north
	
	North rune
	By the power of Earth, I consecrate this Circle
	Facing north

	East rune
	By the power of Water, I consecrate this Circle
	Facing east

	South rune
	By the power of Fire, I consecrate this Circle
	Facing south
	
	West rune
	By the power of Air, I consecrate this Circle
	Facing west
	
	North rune
	Great Elements, I summon you! Appear before me!
	Facing south

	
Ritual of Alchemical Protection

	North skull
	
	//First line
	Dark shadows that hide, from the ether I summon you
	
	//Second line
	By the power of Fire I shackle you

	//Third line
	By the power of Water I still you
	
	//Fourth line
	By the power of Earth I control you
	
	//Fifth line
	By the power of Air I will you
	
	//Sixth line
	Imbue your eternal essence into this familiar aegis
	
	//Seventh line
	Harden and instill it with your dark influence without question
	
	//Eighth line
	And now I bind you forever as the servant to whomever may bear it!
	
Ritual UndoCircle

	North rune
	Great cosmic energy, I humbly thank you for your aid
	Facing north
	
	West rune
	I humbly thank the power of the Air for its energy
	Facing west

	South rune
	I humbly thank the power of the Fire for its energy
	Facing south
	
	
	East rune
	I humbly thank the power of the Water for its energy
	Facing east
	
	North rune
	I humbly thank the power of the Earth for its energy
	Facing north
	
	North rune
	Great Elements, I humbly thank you for your assistance
	Facing south
	
	North rune
	I thank the Universe for this Circle
	Facing south
	
	North rune
	Finally, I return this energy from whence it came
	Facing south
	
	//Final Thanks part 3
	North Rune
	Everything is now as it was
	Facing south
	
	