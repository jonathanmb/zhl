use uo;
use util;

include "include/classes";



function ArcheryAttack( attacker, defender ) 
	var weaponType := cfg[attacker.weapon.objtype];
	//if (!coordDistance (attacker.x, attacker.y, attackx, attacky))
	//endif
	if (!checkForAndDecrementAmmunition( attacker, weaponType))
		return;
	endif
	if (! ValidArcheryWeapon(weaponType))
		return;
	endif
	DoBowAttack(attacker, defender, weaponType);
endfunction 

function DoBowAttack(attacker, defender, weaponType, TO_HIT_SKILLID:= SKILLID_ARCHERY)
	PlayMovingEffectXYZ( attacker.x, attacker.y, attacker.z+9, defender.x, defender.y, defender.z+5, weaponType.ProjectileAnim, 2, 0, 0 );
	PerformAction( attacker, weaponType.Anim);
	sleepms((((100-weaponType.Speed)-Cint(GetDexterity(attacker)/13))/10));
	if (CheckArcheryHit(attacker, defender, TO_HIT_SKILLID))
		PlaySoundEffect( defender, weaponType.hitSound );
	else
		PlaySoundEffect( attacker, weaponType.projectileSound );
		return;
	endif


	//broadcast("weaponType.hitscript : " + weaponType.hitscript + "  dmg mod: " +  attacker.weapon.dmg_mod);
	var hitscript := attacker.weapon.hitscript;
	//Broadcast("hitscript: " + hitscript);
	if (!hitscript)
		//Broadcast("cant fint it!");
		return;
	endif
	var tactics_mod := GetEffectiveSkill(attacker, SKILLID_TACTICS);
	if(GetObjProperty( attacker, CLASSEID_RANGER ) > 0)
		tactics_mod := 50;
	endif
	var damage_multiplier := tactics_mod + 50;
  var rangermod := GetObjProperty( attacker, CLASSEID_RANGER )*0.30;	
  damage_multiplier := damage_multiplier * rangermod;
	var wbasedamage:= RandomDiceRoll( weaponType.damage ); 

	
  damage_multiplier := damage_multiplier + (GetStrength(attacker) * 0.2);
  damage_multiplier := damage_multiplier * 0.01;
  wbasedamage := wbasedamage * damage_multiplier;
  
	
  //broadcast("dmgmp: "+damage_multiplier);
  //broadcast("wbasedamage: "+wbasedamage);
  //broadcast("rangermod: "+rangermod);
  wbasedamage := wbasedamage * (damage_multiplier + rangermod);
  
	if (attacker.weapon.dmg_mod)
		wbasedamage := (wbasedamage + attacker.weapon.dmg_mod);
	endif
	//further bonus damage based on archery skill
	var attackers_archery := GetEffectiveSkill(attacker,TO_HIT_SKILLID);
	var dambonus := 1 + (attackers_archery/75);
	wbasedamage := CInt(wbasedamage * dambonus)*0.90;
  //broadcast("post dmgmp: "+damage_multiplier);
  //broadcast("post wbasedamage: "+wbasedamage);
  //broadcast("postrangermod: "+rangermod);

	//find random piece of armor to hit -- need this for onhit script
	var wornarmor := array;
	foreach li in ListEquippedItems(defender)
		if (li.isA(POLCLASS_ARMOR) && GetObjProperty(li, "OnHitScript"))
			wornarmor.append(li);
		endif
	endforeach
	var def_armor;
	if (len(wornarmor)>0)
		def_armor:=wornarmor[RandomInt(len(wornarmor))+1];
	endif
	if(hitscript == "epicranger")
	start_script (":combat:mainhit", {attacker, defender, attacker.weapon, def_armor, wbasedamage, 0});
	endif
	start_script (":combat:"+hitscript, {attacker, defender, attacker.weapon, def_armor, wbasedamage, 0});
endfunction

function CheckArcheryHit(attacker, defender, TO_HIT_SKILLID:= SKILLID_ARCHERY)
	
	var thiefmod := GetObjProperty( attacker , CLASSEID_THIEF );
	thiefmod := thiefmod*3;
	var rangermod := GetObjProperty( attacker, CLASSEID_RANGER );	
	rangermod := rangermod*6;
	var ppmod := GetEffectiveSkill(attacker,SKILLID_TACTICS);
	ppmod := CInt(ppmod/7);


	var defenders_ar := CInt(1.2 * defender.ar); //based on the idea that monsters dont tend to have more than 100 AR
	var rangeToDef :=Distance (attacker, defender);
	//Broadcast("rangeToDef " + rangeToDef);
	var normalrange := GetObjProperty(attacker.weapon, "NormalRange");
	//Broadcast("normalrange: " + normalrange);
	
	if (!normalrange)
		normalrange:=0;
	endif
	
	if (rangeToDef > normalrange)
		rangeToDef:=rangeToDef + ((rangeToDef-normalrange) * 5); //target past normal range is much harder :)
	endif
	
	var difficulty := defenders_ar + rangeToDef;
	var points:= difficulty * 10;
	if (points > 1300)
		points := 1300;
	endif
	
	//difficulty reduction for dex
	difficulty := difficulty - CInt(attacker.dexterity/3);

	//class reductions
	difficulty := difficulty - thiefmod;
	difficulty := difficulty - rangermod;
	difficulty := difficulty - ppmod;	

	if( defender.isA(POLCLASS_NPC) )

		//random component
		var randdiff := RandomDiceRoll("5d12");
		if (randdiff <=20)
			difficulty := difficulty - randdiff;
		else
			difficulty := difficulty + randdiff;
		endif

	else //is a player - based on the idea that players CAN ACTUALLY REACH MORE THAN 100 AR	

		defenders_ar := defender.ar;
		rangeToDef :=Distance (attacker, defender);
		normalrange := GetObjProperty(attacker.weapon, "NormalRange");

		if (!normalrange)
			normalrange:=0;
		endif

		if (rangeToDef > normalrange)
			rangeToDef:=rangeToDef + ((rangeToDef-normalrange) * 5); //target past normal range is much harder
		endif

		if (rangeToDef < normalrange)
			rangeToDef:=rangeToDef + (normalrange-rangeToDef); //target closer than normal range is a little harder
		endif

		var difficulty := defenders_ar + rangeToDef;
		var points:= difficulty * 10;
		if (points > 1300)
			points := 1300;
		endif
		
		if (defenders_ar>120)
			difficulty:=difficulty*0.9;
		else		
			difficulty:=difficulty*0.95;
		endif
		
		var chance := Random(30);
		var coin := Random(3)+1;
		if(coin<3);
		difficulty := difficulty+chance;
		else
		difficulty := difficulty-chance;
		endif
		
	endif
	
	
	//difficulty cap and floor
	if (difficulty<40)
		difficulty:=Random(40);
	elseif(difficulty>180)
		difficulty:=150+Random(30);
	elseif(difficulty>165)
		difficulty:=140+Random(30);
	elseif(difficulty>150)
		difficulty:=130+Random(25);
	endif

	//Broadcast("difficulty : " + difficulty + "    points : " + points + "   rangeToDef: " + rangeToDef);
	return CheckSkill( attacker, TO_HIT_SKILLID, difficulty, points);
	
endfunction


function checkForAndDecrementAmmunition( who, weaponType )
	var mypack := EnumerateItemsInContainer( who.backpack );
	foreach item in mypack
		if( item.objtype == weaponType.ProjectileType )
			if (SubtractAmount( item, weaponType.Projectile))
				return 1;
			endif
		endif
	endforeach
	return 0;
endfunction

function ValidArcheryWeapon(weaponType)
//returns true if itemdesc elem contains the miniumum requires props for processing
	if (!weaponType.ProjectileAnim || !weaponType.Anim || !weaponType.hitSound || !weaponType.projectileSound ||
	!weaponType.hitscript || !weaponType.damage || !weaponType.Projectile || !weaponType.ProjectileType )
		return 0;
	endif
	return 1;
endfunction