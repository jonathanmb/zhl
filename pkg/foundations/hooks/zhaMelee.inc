use uo;
use polsys;
use util;

include "include/client";
include "include/classes";
include "include/hitscriptinc";
include "include/random";
include "include/attributes";
include "../../pkg/opt/karmafame/karmafame";


var ArmorZoneCfg := ReadConfigFile("::armrzone");
var ArmorZones := GetConfigIntKeys( ArmorZoneCfg );

function MeleeAttack( attacker, defender ) 
	var weaponType := cfg[attacker.weapon.objtype];
	//if (!coordDistance (attacker.x, attacker.y, attackx, attacky))
	//endif
	
	var thiefmod := GetObjProperty( attacker , CLASSEID_THIEF );

	if (thiefmod>= 1)
		DoThiefAttack(attacker, defender, weaponType);
	Else
		DoAttack(attacker, defender, weaponType);
	endif
endfunction

function DoAttack(attacker, defender, weaponType )

	//var attackerweaponType := cfg[attacker.weapon.objtype];
	var defenderweaponType := cfg[defender.weapon.objtype];
	
	//PlayMovingEffectXYZ( attacker.x, attacker.y, attacker.z+9, defender.x, defender.y, defender.z+5, weaponType.ProjectileAnim, 10, 0, 0 );
	if(attacker.npctemplate)
		case(RandomInt(3))
			0: PerformAction( attacker, 4); //Attack1
			1: PerformAction( attacker, 5); //Attack2
			2: PerformAction( attacker, 6); //Attack3
		endcase
	else
		PerformAction( attacker, weaponType.Anim);
	endif
	
	sleep((100-weaponType.Speed)/10);
	var combat_skill_id := GetWeaponSkillID(weaponType.Attribute);
	var defender_combat_skill_id := GetWeaponSkillID(defenderweaponType.Attribute);
	
	//Broadcast("Weapon : " + combat_skill_id);

	if (combat_skill_id == -1)
		return;
	endif
	
	if (CheckMeleeHit(attacker, defender, combat_skill_id, defender_combat_skill_id, weaponType.maxrange))
	
		if(defender.npctemplate)
			PerformAction( defender, 10); //GetHit
		else
			PerformAction( defender, 20); //GetHit
		endif
		
		PlaySoundEffect( defender, weaponType.hitSound );
	else
		PlaySoundEffect( defender, weaponType.MissSound );
		return;
	endif
	
	CombatAdvancement( attacker, weaponType);


	//Broadcast("weaponType.hitscript : " + weaponType.hitscript + "  dmg mod: " +  attacker.weapon.dmg_mod);
	var hitscript := attacker.weapon.hitscript;
	//Broadcast("hitscript: " + hitscript);
	if (!hitscript)
		hitscript := "mainhit";
	endif
	
	var damage_multiplier := GetEffectiveSkill(attacker, SKILLID_TACTICS) + 50;
	var warriormod := GetObjProperty( attacker, CLASSEID_WARRIOR )*0.25;	
  damage_multiplier := damage_multiplier * warriormod;
	var wbasedamage:= RandomDiceRoll( weaponType.damage ); 

	if (attacker.weapon.dmg_mod)
		wbasedamage := wbasedamage + attacker.weapon.dmg_mod;
	endif
	
  damage_multiplier := damage_multiplier + (GetStrength(attacker) * 0.2);
  damage_multiplier := damage_multiplier * 0.01;
  wbasedamage := wbasedamage * damage_multiplier;
 // broadcast("dmgmp: "+damage_multiplier+" wbasedamage: "+wbasedamage);
	
	var OffHand := GetEquipmentByLayer( defender, LAYER_HAND2 );
	if(OffHand && OffHand.isa(POLCLASS_ARMOR) ) //Shield
		AwardSkillPoints( defender, SKILLID_PARRY, 30 );
		
		var parry_chance := GetEffectiveSkill(defender, SKILLID_PARRY) / 200.0;
		
		var Rand := ( ( RandomInt( 1000 ) + 1.0 ) / 1000.0 );
		if( Rand < parry_chance )
			wbasedamage := wbasedamage - OffHand.ar;
		endif
	endif
	
	//find random piece of armor to hit -- need this for onhit script
	var Rand := RandomInt(100) + 1;
	var HitZone;
	
	//Broadcast("RandomZone -> " + Rand);
	
	//Broadcast("TotalZones -> " + ArmorZones.Size());
	
	var Coverage := 0;
	foreach Zone in ArmorZones
		HitZone := ArmorZoneCfg[Zone];
		Coverage := Coverage + HitZone.Chance;
		if(Rand <= Coverage)
			//Broadcast("Found Zone -> " + HitZone.Name);
			break;
		endif
	endforeach
	var EquipInZone := array;
	var Layers := GetConfigStringArray(HitZone, "Layer");
	
	var Equipment;
	foreach Layer in Layers
		Equipment := GetEquipmentByLayer( defender, CInt( Layer ) );
		if(Equipment && Equipment != error)
			EquipInZone.append(Equipment);
		endif
	endforeach
	
	//Broadcast("EquipInZone -> " + EquipInZone.Size() );
	
	var def_armor;
	
	if( Len( EquipInZone ) )
		def_armor := EquipInZone[ RandomInt( Len( EquipInZone ) ) + 1 ];
		
		Rand := RandomInt(100);
		if(Rand == 0)
			def_armor.hp := def_armor.hp - 1;
		endif
		
		//Broadcast("Armor Hit -> " + def_armor.desc);
		
	endif
	
	start_script (":combat:"+hitscript, {attacker, defender, attacker.weapon, def_armor, wbasedamage, 0});

endfunction

function CheckMeleeHit(attacker, defender, attacker_combat_skill_id, defender_combat_skill_id, weapon_range)

	if(!weapon_range)
		weapon_range := 1;
	endif
	
	var weapon_attribute := GetEffectiveSkill( attacker, attacker_combat_skill_id );
	
	var opponent_bonus;
	if(!defender.npctemplate && !attacker.npctemplate) // Player vs. Player
		var Dex := GetDexterity(defender);
		var Stam := GetStamina( defender );
		if( Stam < 50)
			opponent_bonus := Stam + 50.0;
		else
			opponent_bonus := 100.0;
		endif
		//Broadcast("opponent_bonus (PVP) -> " + opponent_bonus);
		opponent_bonus := opponent_bonus + (Dex * 0.35);
		//Broadcast("opponent_bonus (PVP) -> " + opponent_bonus);
	else
		opponent_bonus := GetEffectiveSkill( defender, defender_combat_skill_id );
	endif
	
	var hit_chance := ( ( weapon_attribute + 50.0 ) / ( 2.0 * opponent_bonus + 50.0 ) );
	
	//Broadcast("hit_chance (BASE) -> " + hit_chance);
	/*
	var Behind := 0;
	
	var DeltaX := ( attacker.x - defender.x );
	var DeltaY := ( attacker.y - defender.y );
	
	//Broadcast("DeltaX: " + DeltaX);
	//Broadcast("DeltaY: " + DeltaY);

	
	case(defender.facing)
		0: 	//North
			Behind := ( ( attacker.y - defender.y ) <= weapon_range && attacker.x == defender.x ); break;
		1:
			Behind := ( ( defender.x - attacker.x ) <= weapon_range && ( attacker.y - defender.y ) <= weapon_range ); break;
		2:	//East
			Behind := ( ( defender.x - attacker.x ) <= weapon_range && attacker.y == defender.y ); break;
		3:	
			Behind := ( ( defender.x - attacker.x ) <= weapon_range && ( defender.y - attacker.y ) <= weapon_range ); break;
		4:	//South
			Behind := ( ( defender.y - attacker.y ) <= weapon_range && attacker.x == defender.x ); break;
		5:
			Behind := ( ( attacker.x - defender.x ) <= weapon_range && ( defender.y - attacker.y ) <= weapon_range ); break;
		6:	//West
			Behind := ( ( attacker.x - defender.x ) <= weapon_range && attacker.y == defender.y ); break;
		7:
			Behind := ( ( attacker.x - defender.x ) <= weapon_range && ( attacker.y - defender.y ) <= weapon_range ); break;
		default:
			break;
	endcase
	
	
	if(Behind)
		hit_chance := hit_chance + 0.10;
		
	endif
	
	*/
	
	var class_modifier := 0;
	
	var WarriorLevel	:= 	CInt ( GetObjProperty( attacker , CLASSEID_WARRIOR ) );
	var ThiefLevel	:= 	CInt ( GetObjProperty( attacker , CLASSEID_THIEF ) );
	
	if ( WarriorLevel > 0 )
		class_modifier := ( ( WarriorLevel * 6.0 ) / 100.0 ) * hit_chance; //6% extra per level
	elseif ( ThiefLevel > 0 )
		class_modifier := ( ( ThiefLevel * 4.0 ) / 100.0 ) * hit_chance; //4% extra per level
	endif

	hit_chance := hit_chance + class_modifier;
	
	//Broadcast("hit_chance (CLASS+) -> " + hit_chance);
	

	
	if(hit_chance > 0.90)
		hit_chance := 0.90;
	elseif(hit_chance < 0.10)
		hit_chance := 0.10;
	endif

	//Broadcast("hit_chance (CLAMPED) -> " + hit_chance);
	
	var Rand := ( ( RandomInt( 1000 ) + 1.0 ) / 1000.0 );
	
	//Broadcast("RandomFloat -> " + Rand);
	
	return ( hit_chance > Rand );
	
endfunction

function CombatAdvancement( attacker, weapon )

	if( weapon )
		var skillid := FindSkillidByIdentifier(weapon.attribute);
		AwardSkillPoints( attacker,skillid, FameExperiencePointModifier(attacker, skillid, 20));
	else
		AwardSkillPoints( attacker, SKILLID_WRESTLING, FameExperiencePointModifier(attacker, SKILLID_WRESTLING, 20));
	endif

	AwardSkillPoints( attacker, SKILLID_TACTICS, FameExperiencePointModifier(attacker, SKILLID_TACTICS, 20) );

endfunction

function FindSkillidByIdentifier(skid)
  var retval;
  case(skid)
    "Wrestling":	retval := SKILLID_WRESTLING;
    "Fencing":		retval := SKILLID_FENCING;
    "Swords":		retval := SKILLID_SWORDSMANSHIP;
    "Swordsmanship":	retval := SKILLID_SWORDSMANSHIP;
    "Mace":		retval := SKILLID_MACEFIGHTING;
    "Macefighting":	retval := SKILLID_MACEFIGHTING;
    "Archery":		retval := SKILLID_ARCHERY;
  endcase 
  return retval;
endfunction

function DoThiefAttack(attacker, defender, weaponType )
	var attackerweaponType := cfg[attacker.weapon.objtype];
	
	//PlayMovingEffectXYZ( attacker.x, attacker.y, attacker.z+9, defender.x, defender.y, defender.z+5, weaponType.ProjectileAnim, 10, 0, 0 );
	PerformAction( attacker, weaponType.Anim);
	sleep((100-weaponType.Speed)/10);
	var combat_skill_id := GetWeaponSkillID(weaponType.Attribute);
	
	//Broadcast("Combat skill id: " + combat_skill_id);

	if (combat_skill_id == -1)
		return;
	endif
					
	if (CheckThiefMeleeHit(attacker, defender, combat_skill_id))
		PlaySoundEffect( defender, weaponType.hitSound );
	else
		PlaySoundEffect( attacker, weaponType.projectileSound );
		return;
	endif


	//Broadcast("weaponType.hitscript : " + weaponType.hitscript + "  dmg mod: " +  attacker.weapon.dmg_mod);
	var hitscript := attacker.weapon.hitscript;
	//Broadcast("hitscript: " + hitscript);
	if (!hitscript)
		//Broadcast("cant fint it!");
		return;
	endif
	var wbasedamage:= RandomDiceRoll( weaponType.damage ); 
	if (attacker.weapon.dmg_mod)
		wbasedamage := wbasedamage + attacker.weapon.dmg_mod;
	endif
	//further bonus damage based on archery skill - temporarily removed pending testing
	
	var thiefmod := GetObjProperty( attacker , CLASSEID_THIEF );
	if (thiefmod>=1)
	var attackers_skill := GetEffectiveSkill(attacker,combat_skill_id);
	var dambonus := 1 + (attackers_skill/130);
	wbasedamage := CInt(wbasedamage * dambonus);
	endif
	
	//find random piece of armor to hit -- need this for onhit script
	var wornarmor := array;
	foreach li in ListEquippedItems(defender)
		if (li.isA(POLCLASS_ARMOR) && GetObjProperty(li, "OnHitScript"))
			wornarmor.append(li);
		endif
	endforeach
	var def_armor;
	if (len(wornarmor)>0)
		def_armor:=wornarmor[RandomInt(len(wornarmor))+1];
	endif
	start_script (":combat:"+hitscript, {attacker, defender, attacker.weapon, def_armor, wbasedamage, 0});
endfunction

function CheckThiefMeleeHit(attacker, defender, combat_skill_id)
	
	var thiefmod := GetObjProperty( attacker , CLASSEID_THIEF );
	if (thiefmod>= 1)
	thiefmod := (thiefmod * 4)+10;
	endif
	
	var warriormod := GetObjProperty( attacker , CLASSEID_WARRIOR );
	warriormod := (warriormod * 3)+10;
	
	var defenders_ar := CInt(1.3 * defender.ar);
	
	var difficulty := defenders_ar;
	var points:= difficulty * 10;
	if (points > 1300)
		points := 1300;
	endif

	points:=FameExperiencePointModifier(attacker, combat_skill_id, points);
	points:=DKSMod(attacker, combat_skill_id, points);

	//random component
	var randdiff := RandomDiceRoll("5d12");
	if (randdiff <=30)
		difficulty := difficulty - randdiff;
	else
		difficulty := difficulty + (randdiff-20);
	endif

	//Broadcast("Random modifier result: " + difficulty);

	//difficulty reduction for dex
	difficulty := difficulty - CInt(attacker.dexterity/6);
	//difficulty reduction for level
	difficulty := difficulty - thiefmod;
	difficulty := difficulty - warriormod;

	if( defender.isA(POLCLASS_NPC) )
		difficulty := difficulty + 25;
	else
		var pvp_ar := defender.ar;
		var skill := GetEffectiveSkill( attacker, combat_skill_id );
		var adex := attacker.dexterity;
		var ddex := defender.dexterity;
		var chance := Random(99)+1;
		var defcalc := (pvp_ar + (ddex/3) + Random(80)+20);
		var attcalc := ((skill+20) + (adex/3) + (Random(50)));

		//Broadcast("Skill level: " + skill);
		//Broadcast("DEFCALC: " + defcalc);
		//Broadcast("ATTCALC: " + attcalc);
		//Broadcast("CHANCE: " + chance);		

		if (chance > 94)
		return 1;
		endif
		if (chance < 11)
		return 0;
		endif

		var resultcalc := defcalc - attcalc;
		//Broadcast("RESULTCALC: " + resultcalc);
		if(resultcalc > 100)
			If(Random(100)>94)
			AwardSkillPoints( attacker, combat_skill_id, points/4 );
			return 1;
			else
			return 0;
			endif
		elseif(resultcalc>94)
			If(Random(100)<3)
			AwardSkillPoints( attacker, combat_skill_id, points/4 );
			return 1;
			else
			return 0;
			endif
		elseif(resultcalc<89)
			if(resultcalc<11)
			resultcalc := 11;
			endif
			if(resultcalc<chance)
			AwardSkillPoints( attacker, combat_skill_id, points/4 );
			return 1;
			endif
		endif
		return 0;

	endif
		
	//Broadcast("Difficulty balancer result: " + difficulty);
		
		if (difficulty<1)
			difficulty:=1;
		elseif(difficulty>150)
			difficulty:=150;
		endif
	//Broadcast("Difficulty CAPPER: " + difficulty);

	//Broadcast("difficulty : " + difficulty + "    points : " + points + "   combat_skill_id: " + combat_skill_id);
	return CheckSkill( attacker, combat_skill_id, difficulty, points);
	
endfunction

function GetMeleeWeaponSkillID(weaponTypeAttribute)
	if (weaponTypeAttribute=="Mace")
		return SKILLID_MACEFIGHTING;
	elseif	(weaponTypeAttribute=="Swords")
		return SKILLID_SWORDSMANSHIP;
	elseif	(weaponTypeAttribute=="Fencing")
		return SKILLID_FENCING;
	
	/*elseif	(weaponTypeAttribute=="Wrestling")
		return 43;
	*/
	endif
	return 43; 
endfunction

function DKSMod(attacker, combat_skill_id, points)
	
	if( attacker.IsA(POLCLASS_NPC) )	
		return points;
	endif
	
	//dont do anything if skill set to decrease or locked
	var skills_state_array   := GetObjProperty( attacker, "SkillsState" );
	if( !skills_state_array )
		skills_state_array := array;
	endif
	
	var skill_state := skills_state_array[combat_skill_id+1];
	if( skill_state == "d" || skill_state == "l" )
		return points;
	endif

	var dks := GetObjProperty(attacker, "dks");
	if (dks<1)
		return points;
	elseif (dks>1)
		return points;
	endif
	
	//at this stage skills up to 119 raise by DKS
	var skill := GetEffectiveSkill( attacker, combat_skill_id );
	if (skill>119)
		return points;
	endif

	var mod := 1.5;

	return CInt(points*mod);

endfunction